<?php
/**
 * The development database settings. These get merged with the global settings.
 */

return array(
	'default' => array(
		'connection'  => array(
//			'dsn'        => 'mysql:unix_socket=/Application/MAMP/tmp/mysql/mysql.sock;dbname=fuel_dev',
			'dsn'        => 'mysql:host=127.0.0.1;dbname=fuel_dev',
			'username'   => 'root',
			'password'   => 'root',
		),
	),
);
