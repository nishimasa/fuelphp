<?php

class Controller_Vote extends Controller{
	public function action_login(){
		if(Input::post('id')!= ''){
			if(Auth::login(Input::post('id'),Input::post('password'))){
				Response::redirect('vote/view');
			}
		}

		$image = Model_Image::find('1');
		$data = array('image' => $image);

		return Response::forge(View::forge('vote/login',$data));
	}
	public function action_logout(){
		Auth::logout();
		Response::redirect('vote/login');		
	}
	public function action_view(){
		if(! Auth::check()){
			Response::redirect('vote/login');
		}
		$images = Model_Image::find('all');
		$data = array('images' => $images);
		return Response::forge(View::forge('vote/view',$data));
	}
}


?>