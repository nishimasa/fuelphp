<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<title>ログイン</title>
		<style>
		body{
			margin:40px;
			text-align:center;
			background-image:url("../assets/img/<?php echo $image['file_name']?>");
			background-repeat:no-repeat;
			background-size: cover;
		}
		.l_form{
			width:40%;
			margin:30% auto;
			background-color:#fff;
			opacity: 0.6;
			border-radius:10px;  
		}
		.l_form h1{
			padding-top:8%;
		}
		input {
			margin:2%;
			padding: 0.5em;
		}
		#form_login{
			display:inline-block;
			margin-bottom:5%;
		}


		</style>
	</head>
	<body>
		<div class = "l_form">
			<?php echo Form::open('vote/login')?>
				<h1>ログイン<br></h1>
				ユーザーID:<?php echo Form::input('id',''); ?>
				<br>
				パスワード:<?php echo Form::input('password',''); ?>
				<br>
				<?php echo Form::submit('login','login');?>
			<?php echo Form::close(); ?>
		</div>
	</body>
</html>