<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<title>一覧画面</title>
		<style>
		body{
			margin:40px;
		}
		li{
			list-style:none;
			display:table-cell;	
			padding:1%;
		}
		li:nth-child(3){
			
		}
		img{
			width: 100%;

		}

		</style>
		<script
			src="https://code.jquery.com/jquery-2.2.4.js"
			integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
			crossorigin="anonymous">	
		</script>
		<script>
			$(function(){
				$('input.vote').on('click',function(){
					var id = $(this).data('id');
					$.ajax({
						url:"<?php echo Uri::create('api/vote.json'); ?>",
						type:"POST",
						data:{id:id},
						dataType:"json"
					}).done(function(data){
						alert(data.message);
						location.reload();
					}).fail(function(data){
						alert('処理に失敗しました');
					});

				});
			});
		</script>
	</head>
	<body>
		一覧画面
		<?php echo Html::anchor('vote/logout','ログアウト');?>
		<div>
		<ul>
		<?php foreach($images as $img): ?>
			<li>
				<?php echo Asset::img($img['file_name']); ?>
				<br>
				<?php echo $img['votes']; ?>
				<input type="button" class="vote" data-id="<?php echo $img['id']; ?>" value="投票する">
			</li>
		<?php endforeach; ?>
		</ul>
		</div>
	</body>
</html>