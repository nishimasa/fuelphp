<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<title>一覧画面</title>
		<style>
		body{
			margin:40px;
		}
		</style>
	</head>
	<body>
		一覧画面
		<?php echo Html::anchor('admin/logout','ログアウト');?>
		<br>
		<?php echo Html::anchor('admin/upload','アップロード')?>

		<div>
		<ul>
		<?php foreach($images as $img): ?>
			<li>
				<?php echo Asset::img($img['file_name']); ?>
				<span class="votes"><?php echo $img['votes']?></span>
			</li>
		<?php endforeach; ?>
		</ul>
		</div>
	</body>
</html>